const path = require('path');
const express = require('express');
const socketIO = require('socket.io');

const {generateMessage} = require('./utils/message');
const {isRealString} = require('./utils/validation');
const {Users} = require('./utils/users');

const publicPath = path.join(__dirname, '../public');
const port = process.env.PORT || 3000;
var app = express();
var http = require('http').Server(app);
// var server = http.createServer(app);
var io = socketIO(http);
var users = new Users();

app.use(express.static(publicPath));

io.on('connection', function (socket) {
    console.log('New user connected');
    
    
    socket.on('join', (params, callback) => {
        if (!isRealString(params.name) || !isRealString(params.room)) {
            return callback('Name and room are required.');
        }
        socket.join(params.room);
        users.removeUser(socket.id);
        users.addUser(socket.id, params.name, params.room);
        io.to(params.room).emit('updateUserList', users.getUserList(params.room));
        socket.emit(
            'newMessage', 
            generateMessage('Admin', `Welcome to ${params.room}!`)
        );
        socket.broadcast.to(params.room).emit(
            'newMessage', 
            generateMessage('Admin', `${params.name} has joined !!`)
        );

        callback();
    });

    socket.on('createMessage', function (message, callback) {
        var user = users.getUser(socket.id);

        if(user && isRealString(message.text)) {
            io.to(user.room).emit('newMessage', generateMessage(user.name, message.text));
        }

        callback();
    });
    
    socket.on('disconnect', function (socket) {
        let user = users.removeUser(socket.id);

        if (user) {
            io.to(user.room).emit('updateUserList', users.getUserList(user.room));
            io.to(user.room).emit(
                'newMessage', 
                generateMessage('Admin', `${user.name} has left.`)
            );
        }
    });
});


http.listen(port, () => {
    console.log(`Server is up on ${port}`);
});

// var app = require('express')();
// var http = require('http').Server(app);
// var io = require('socket.io')(http);

// // app.get('/', function (req, res) {
// //     res.sendFile(__dirname + '/index.html');
// // });


// // http.listen(3000, function () {
// //     console.log('listening on *:3000');
// // });
// const path = require('path');
// const express = require('express');
// const publicPath = path.join(__dirname, '../public');
// const port = process.env.PORT || 3000;
// app.use(express.static(publicPath));

// http.listen(3000, () => {
//     console.log(`Server is up on 3000`);
// });