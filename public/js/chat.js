var socket = io();

function scrollToBottom() {
    var messages = jQuery('#messages');
    var newMessage = messages.children('li:last-child');

    var clientHeight = messages.prop('clientHeight');
    var scrollTop = messages.prop('scrollTop');
    var scrollHeight = messages.prop('scrollHeight');
    var newMessageHeight = newMessage.innerHeight();
    var lastMessageHeight = newMessage.prev().innerHeight();

    if (clientHeight + scrollTop + scrollHeight + newMessageHeight + lastMessageHeight >= scrollHeight ) {
            messages.scrollTop(scrollHeight);
    }
}

socket.on('connect', function () {
    var params = jQuery.deparam(window.location.search);
    socket.emit('join', params, function (err) {
        console.log(params);
        if (err) {
            alert(err);
            window.location.href = '/';
        }else{
            console.log('no');
        }
    });
});

socket.on('disconnect', function () {
    // console.log('Disconnected from server');
    // alert('Disconnected from server');
});

socket.on('updateUserList', function (users) {
    let ol = jQuery('<ol></ol>');
    // console.log(users);
    users.forEach(function (user) {
        ol.append(jQuery('<li></li>').text(user));
    });

    jQuery('#users').html(ol);
});

socket.on('newMessage', function (message) {
    var formattedTime = moment(message.createdAt).format('h:mm a');
    var template = jQuery('#message-template').html();
    var html = Mustache.render(template, {
        text:message.text,
        from:message.from,
        createdAt:formattedTime
    });
    // var li = jQuery('<li></li>');
    // li.text(`${message.from} (${formattedTime}): ${message.text}`);

    jQuery('#messages').append(html);
    scrollToBottom();
});

jQuery('#message-form').on('submit', function (e) {
    e.preventDefault();
    let messageTextBox = jQuery('[name=message]');
    jQuery('#send').text('Sending');
    socket.emit('createMessage', {
        text: messageTextBox.val()
    }, function(data){
        messageTextBox.val('')
        jQuery('#send').text('Send');
    });
});